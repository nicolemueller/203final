
public class Sample implements Comparable<Sample>{
	private Integer number;
	
	private int sign;
	public Sample(Integer input) {
		number = input;
	}
	public void changeOrder(int sign) {
		this.sign = sign;
	}
	public int getNumber() {
		return number;
	}
	public int compareTo(Sample in) {
		return sign * (number - in.getNumber());
	}
	
	public String toString() {
		return number.toString();
	}

	public static void main(String[] args) {
		PriorityQueue<Sample> pq = new PriorityQueue<Sample>;
		pq.add(new Sample(6));
		pq.add(new Sample(15));
		pq.add(new Sample(-5));
		pq.add(new Sample(30));
		pq.add(new Sample(25));
		pq.add(new Sample(16));
		
		while(pq.size()>0) {
			
		}

	}
	
}
