import java.util.*;

 class DnDcharacter implements Comparable{
	
	String name;
	Integer skill;
	
	public int compareTo(DnDcharacter c) {
		return(skill - c.skill); //add - sign to do the opposite 
	}
	
	public DnDchanracter(String _name, Integer _skill) {
		name = _name;
		skill = _skill;
	}
	public String toString() {
		return (name + "(" + skill + ")");
	}
}
public class DnDSample {
	
	
	public static void main(String[] args) {
		
		HashMap<String, HashMap<String, Integer>> hm = new HashMap<String, HashMap<String, Integer>>();
		
		hm.put("Ant man - Barbossa", new HashMap<String, Integer>());
		hm.get("Ant man - Barbossa").put("Strength", 25);
		hm.get("Ant man - Barbossa").put("Wisdom", 15);
		hm.get("Ant man - Barbossa").put("Intelligence", 30);
		hm.get("Ant man - Barbossa").put("Charisma", 555);
		hm.get("Ant man - Barbossa").put("Constitution", 9876567);
		
		hm.put("Mani Kent - Cucumber", new HashMap<String, Integer>());
		hm.get("Mani Kent - Cucumber").put("Strength", 17);
		hm.get("Mani Kent - Cucumber").put("Wisdom", 22);
		hm.get("Mani Kent - Cucumber").put("Intelligence", 9);
		hm.get("Mani Kent - Cucumber").put("Charisma", 5);
		hm.get("Mani Kent - Cucumber").put("Constitution", 728);
		
		hm.put("Pickle Rick - Pickle", new HashMap<String, Integer>());
		hm.get("Pickle Rick - Pickle").put("Strength", 4);
		hm.get("Pickle Rick - Pickle").put("Wisdom", 34543);
		hm.get("Pickle Rick - Pickle").put("Intelligence", 5);
		hm.get("Pickle Rick - Pickle").put("Charisma", 345);
		hm.get("Pickle Rick - Pickle").put("Constitution", 634);
		
		PriorityQueue<DnDcharacter> pq = new PriorityQueue<DnDcharacter>();
		
		String skill = "Wisdom";
		Set<String> set = hm.keySet();
		for(String name : set)	
			pq.add(new DnDcharacter(name, hm.get(name).get(skill)));
		
		while(pq.size()>0) {
			System.out.println(pq.poll());
		}
			
			System.out.println(hm);
	}

}
