package test;

public class ChristmasTree {
	
		 
		 ChristmasTree() {}
		 
		 public void drawTree(int cur_level, int max_level) {
		    
		       // recursive call to draw the smaller levels first
		       drawTree(cur_level, max_level);
		 
		       // print spaces
		       for (int i = 0; i < max_level - cur_level; i++)
		          System.out.print(" ");
		 
		       // print stars
		       for (int i = 0; i < cur_level * 2 - 1; i++)
		          System.out.print("*");
		 
		       // move the cursor to the new line
		       System.out.println();
		    
		 }
		 
		 public static void main(String args[]) {
		    ChristmasTree ct = new ChristmasTree();
		    int level = 6;
		    ct.drawTree(level, level);
		 }
		 
}
