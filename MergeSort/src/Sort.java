/**
 * This is an example of sorting and Integer array
 * @author nmuel
 *
 */
public class Sort {

	public Sort() {

	}
	/**
	 *  This method takes care of sorting the input array, 
	 *  utilizing a common dive-and-conquer algorithms called merge sort
	 * @param foo is the input array to be sorted
	 * @param start the start position of the merge algorithm left side of the array
	 * @param mid the middle position used in the merge algorithm
	 * @param end the end position used in the merge algorithm 
	 */
	
	private void merge(Integer[] foo, int start, int mid, int end) {
		
		Integer[] temp = new Integer[end - start + 1];
		//start = 0, mid = 1, end = 3
			//5, 7,    -7, 4,          10, 1
			//5, 7,    -7, 4,          10, 1	
			
		//int j = mid + 1;
		//int k = 0;
		//for(int i = start; i<= mid; i++) {
			//if (foo[i] < foo[j]) {
			//	temp[k++] = foo[i];
		//	}
			//else {
				//temp[k++] = foo[j];
		//	}
		//}
		
		int i = start;
		int j = mid + 1;
		int k = 0;
		while (j != end + 1 && i != mid +1) {
			if(foo[i] < foo[j]) {
				temp[k++] = foo[i++];
			}
			else {
				temp[k++] = temp[j++];
			}
		}
		
		while( i != mid + 1) {
			temp[k++] = foo[i++];
		}
		while( j != end + 1) {
			temp[k++] = foo[j++];
		}
		
		int y = start;
		for (int x = 0; x < temp.length; x++)
			foo[y++] = temp[x];
	}
	
	public void mergeSort(Integer[]blah, int start, int end) {
		
		if(start < end) {
			int mid = (start + end) /2;
			mergeSort(blah, start, mid);
			mergeSort(blah, mid+1, end);
			merge(blah, start, mid, end);
		}
	}
	
	public void sort(Integer[] input) {
		mergeSort(input, 0, input.length -1);
	}
	
	public static void main(String[] args) {
		Integer[] input = {5, 7, -7, 4, 10, 1};
		
		Sort s = new Sort();
		s.sort(input);
		
		for(int i = 0; i < input.length; i++){
			System.out.print(input[i] + " ");
		}

	}

}
