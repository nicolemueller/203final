package List;

public class TestLinkedList {

	public static void main(String[] args) {
		LinkedList<Integer> ll = new LinkedList<Integer>();
		
		try {
			ll.add(5);
			ll.add(6, 0);
			ll.add(-10, 2);
			ll.add(-100, 3);
			
			Integer k = ll.remove(2);
			System.out.println("Removed element is " + k);
			
			ll.display();			
		}
		catch(IndexOutOfBoundsException ie)
		{
			System.out.println(ie.getMessage());
		}
		
	}

}
