
public class Node {
	int item;
	Node leftChild;
	Node rightChild;
	Node parent;
	
	Node(int item){
		this.item = item;
	}
}
