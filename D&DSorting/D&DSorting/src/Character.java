
public class Character {

	String name;
	int strength, dexterity, constitution, intelligence, wisdom, charisma;
	
	Character(String name, int strength, int dexterity, int constitution, int intelligence, int wisdom, int charisma){
		this.name = name;
		this.strength = strength;
		this.dexterity = dexterity;
		this.constitution = constitution;
		this.intelligence = intelligence;
		this.wisdom = wisdom;
		this.charisma = charisma;
	}
}
