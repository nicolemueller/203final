import java.util.ArrayList;
import java.util.Scanner;


public class Main {

	public static void main(String[] args) {
		
		Scanner in = new Scanner (System.in);
		Item item;
		String itemName;
		double itemPrice;
		int quantity;
		
		ArrayList<Item> Cart = new ArrayList<Item>();
		ShoppingCart cart1 = new ShoppingCart();
		
		System.out.println("Enter the name of the item you wish to purchase: ");
		itemName = in.nextLine();
		
		System.out.println("Enter the price of the item you are purchasing: ");
		itemPrice = in.nextDouble();
		
		System.out.println("Enter the quantity you wish to purchase: ");
		quantity = in.nextInt();
		
		cart1.addToCart(itemName, itemPrice, quantity);
		
		System.out.println(cart1);
		
		

	}

}
