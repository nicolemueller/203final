import java.text.NumberFormat;

public class ShoppingCart {
	Item [] cart;
	int itemCount; // number of items in the cart
	double totalPrice; //price for the items in the cart
	int cartCapacity; //how big the cart currently is
	
	public ShoppingCart(){
      cartCapacity = 5;
      cart = new Item[cartCapacity];
      itemCount = 0;
      totalPrice = 0.0;
    }
	
	public void addToCart(String itemName, double price, int quantity) {
		Item temp = new Item(itemName, price, quantity);
		totalPrice += (price * quantity);
		cart[itemCount] = temp;
		itemCount += 1;
	}
	
	public String toString()
    {
      NumberFormat fmt = NumberFormat.getCurrencyInstance();
      
      System.out.println();
      String contents = "\nShopping Cart\n";
      contents += "\nItem\t\tUnit Price\tQuantity\tTotal\n";

      for (int i = 0; i < itemCount; i++)
          contents += cart[i].toString() + "\n";

      contents += "\nTotal Price: " + fmt.format(totalPrice);
      contents += "\n";

      return contents;
    }
	
	private void increaseSize()
    {
        Item[] temp = new Item[cartCapacity+3];
        for(int i=0; i < cartCapacity; i++){
            temp[i] = cart[i];
        }
        cart = temp; 
        temp = null;
        cartCapacity = cart.length;
    }

}
