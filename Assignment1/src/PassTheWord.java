import java.util.*;

public class PassTheWord {

	public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in);
	HashMap<String, String> users = new HashMap<String, String>();
	boolean doAgain = true;
	
		while(doAgain) {
		System.out.println("If you would like to sign up, please enter 1. If you would like to login, please enter 2");
		int signOrLog = scan.nextInt();
		if(signOrLog ==1)
		{
			SignUp(users);
		}
		else
		{
			Login(users);
		}
	
		System.out.println("\nWould you like to go back to the main menu? Please enter 1 for yes or 2 for no");
		int mainMenu = scan.nextInt();
	
		if(mainMenu==1)
		{
			doAgain = true;
		}
		else
		{
			doAgain = false;
		}	
		}
	
	}
	public static void SignUp(HashMap users) {
	
		Scanner scan = new Scanner(System.in);
	
		System.out.println("Please enter a username: ");
		String tempUser = scan.nextLine();
		if(users.containsKey(tempUser))
		{
			System.out.println("That username is already taken. Please enter a new one");
			SignUp(users);
		}
		else
		{	
			CreatePassword(users, tempUser);
		}
	}

	public static void CreatePassword(HashMap users, String tempUser)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter a password that has at least one uppercase letter, one lower case letter, one digit, and be at least 6 characters long: ");
		String tempPass = scan.nextLine();
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{6,}";
		if(tempPass.matches(pattern))
		{
			users.put(tempUser, tempPass);
			System.out.println("You have now made a new account");
		}
		else
		{
			System.out.println("Please enter a new password that meets the requirements");
			CreatePassword(users, tempUser);
		}
	}
	public static void Login(HashMap users) {
		
		Scanner scan = new Scanner(System.in);	
	
		System.out.println("Please enter your username: ");
		String checkUser = scan.nextLine();
		if(users.containsKey(checkUser))
		{
			PasswordLogin(users, checkUser);
		}
		else
		{
			System.out.println("Incorrect username. Please try again");
			Login(users);
		}
	}
	
	public static void PasswordLogin(HashMap users, String checkUser)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Please enter your password: ");
		String checkPass = scan.nextLine();
		if(users.get(checkUser).equals(checkPass))
		{
			System.out.println("Login success! Don't cry, but Arcadia has all of your money :/ \n");
		}
		else
		{
			System.out.println("Incorrect password. Please try again.");
			PasswordLogin(users, checkUser);
		}
	}
}
